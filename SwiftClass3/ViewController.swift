//
//  ViewController.swift
//  SwiftClass3
//
//  Created by Igor Maldonado Floôr on 24/02/18.
//  Copyright © 2018 Igor. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    var selectedCarName = ""
    
    var cars: [String] = ["Ferrari", "Lamborghini", "Maserati", "Porsche", "Mustang", "Camaro", "NissanNX", "Impala", "Skyline"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(VehicleTableViewCell.self, forCellReuseIdentifier: "VehicleTableViewCell")
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: String(describing: VehicleTableViewCell.self), bundle: nil), forCellReuseIdentifier: "VehicleTableViewCell")
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cars.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = self.tableView.dequeueReusableCell(withIdentifier: "VehicleTableViewCell") as? VehicleTableViewCell
        
        if (cell == nil) {
           cell = VehicleTableViewCell()
        }
        
        cell?.numberLabel.text = "\(indexPath.row)"
        cell?.nameLabel.text = cars[indexPath.row]
        cell?.carImage.image = UIImage(named: cars[indexPath.row])
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedCarName = cars[indexPath.row]
        self.performSegue(withIdentifier: "fullScreenSegue", sender: nil)
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "fullScreenSegue" {
            let fullScreenViewController = segue.destination as? FullScreenViewController
            fullScreenViewController?.carName = self.selectedCarName
        }
    }

}

